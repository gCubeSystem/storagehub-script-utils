package org.gcube.data.access.storagehub.scripting;

import org.apache.jackrabbit.api.JackrabbitSession;

public interface AbstractScript {
	
	String run(JackrabbitSession session, ScriptParameter prameters, ScriptUtil scriptUtil) throws Exception;
	
}
