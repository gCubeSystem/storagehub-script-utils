package org.gcube.data.access.storagehub.scripting;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.gcube.common.storagehub.model.exceptions.BackendGenericError;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.items.nodes.Content;
import org.gcube.common.storagehub.model.storages.StorageBackendFactory;

public interface ScriptUtil {
	
	Item getItem(Node node, List<String> excludes) throws RepositoryException, BackendGenericError ;
	
	List<Item> getChildren(Predicate<Node> checker, Node parent, List<String> excludes, boolean showHidden, Class<? extends Item> nodeTypeToInclude) throws RepositoryException, BackendGenericError;

	void removeNodes(Session ses, List<Item> itemsToDelete) throws RepositoryException, StorageHubException;

	Collection<StorageBackendFactory> getStorageBackendHandler();
	
	public void updateContentNode(Content content, Node node) throws Exception;

	Node createInternalFolder(Session ses, String name, String description, String userOwner, String parentNodeIdentifier) throws StorageHubException;
}
