package org.gcube.data.access.storagehub.scripting;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ScriptParameter {

	private Map<String, Object> params = new HashMap<>();
	
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
	public Map<String, Object> getParams(){
		return Collections.unmodifiableMap(params);
	}
}
